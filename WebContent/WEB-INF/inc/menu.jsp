<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<hr>
<ul>
	<c:choose>
		<c:when test="${empty sessionScope.admin }">
			<div class="navbar">
				<a href="<c:url value='/login'/>">Se connecter</a>
			</div>
		</c:when>
		<c:otherwise>
			<div class="navbar">
				<a href="<c:url value='/clients/add'/>">Ajouter</a>
				<a href="<c:url value='/clients/list'/>">Lister</a>
				<a href="<c:url value='/logout'/>">Se déconnecter</a>
			</div>
		</c:otherwise>
	</c:choose>
</ul>
<hr>