<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Accueil</title>
	<link rel="stylesheet" href="<c:url value='/style.css'/>">
</head>
<body>
	<c:import url="inc/entete.jsp"/>
	<c:import url="inc/menu.jsp"/>
	<h1>Page d'accueil</h1>
	<p>Bienvenue dans notre application de gestion d'albums</p>
	<c:choose>
		<c:when test="${ empty albums }">
			<p>Aucun album</p>
		</c:when>
		<c:otherwise>
			<table border="1" cellspacing="0">
				<tr>
					<th>Nom</th>
					<th>Description</th>
					<th>Propriétaire</th>
				</tr>
				<c:forEach items="${ albums }" var="album">
					<tr>
						<td><c:out value="${ album.nom }"/></td>
						<td><c:out value="${ album.description }"/></td>
						<td><c:out value="${ album.user.username }"/></td>
						<td> <a href="<c:url value='/albums/update?id=${ album.id }'/>">Afficher</a> </td>
					</tr>
				</c:forEach>
			</table>
		</c:otherwise>
	</c:choose>
	
</body>
</html>