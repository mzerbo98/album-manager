package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.DAOException;
import dao.UserDAO;
import metier.forms.AuthenticationForm;

/**
 * Servlet implementation class Authentification
 */
@WebServlet({ "/login", "/logout" })
public class Authentification extends HttpServlet
{
	private static final long	serialVersionUID	= 1L;
	private static final String	VUE_LOGIN			= "/WEB-INF/login.jsp";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		String requestedUrl = request.getServletPath();

		if (requestedUrl.equals("/login"))
		{
			getServletContext().getRequestDispatcher(VUE_LOGIN).forward(request,
					response);
		}
		else
		{
			HttpSession session = request.getSession();
			session.invalidate();
			response.sendRedirect(request.getContextPath());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		AuthenticationForm form = new AuthenticationForm(request);
		User utilisateur;
		try {
			utilisateur = form.connect();
		} catch (DAOException e) {
			e.printStackTrace();
			utilisateur = null;
		}
		if (utilisateur != null)
		{
			request.getSession().setAttribute("user", utilisateur);
			try {
				request.getSession().setAttribute("userAlbums", UserDAO.getUserAlbums(utilisateur));
			} catch (DAOException e) {
				e.printStackTrace();
			}
			response.sendRedirect(request.getContextPath() + "/");
		}
		else
		{
			request.setAttribute("form", form);
			getServletContext().getRequestDispatcher(VUE_LOGIN).forward(request,
					response);
		}
	}

}
