package beans;

import java.util.Date;

import lombok.Data;

public @Data class Image {
	
	private int id;
	private String titre;
	private String description;
	private String keywords;
	private Date creation;
	private Date modification;
	private int hauteur;
	private int largeur;
	private String url;
	private Album album;

	public Image() {
		super();
	}
	
	public Image(int id) {
		super();
		this.id = id;
	}
	
	public Image(int id, String titre, String description, String keywords, Date creation, Date modification,
			int hauteur, int largeur, String url,Album album) {
		super();
		this.id = id;
		this.titre = titre;
		this.description = description;
		this.keywords = keywords;
		this.creation = creation;
		this.modification = modification;
		this.hauteur = hauteur;
		this.largeur = largeur;
		this.url = url;
		this.album = album;
	}

	public Image(String titre, String description, String keywords, Date creation, Date modification, int hauteur,
			int largeur, String url, Album album) {
		super();
		this.titre = titre;
		this.description = description;
		this.keywords = keywords;
		this.creation = creation;
		this.modification = modification;
		this.hauteur = hauteur;
		this.largeur = largeur;
		this.url = url;
		this.album = album;
	}
}
