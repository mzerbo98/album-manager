package beans;

import java.util.ArrayList;

import lombok.Data;

public @Data class Album {
	
	private int id;
	private String nom;
	private String description;
	private boolean prive;
	private ArrayList<Image> images;
	private User user;
	
	public Album() {
		super();
	}
	
	public Album(int id) {
		super();
		this.id = id;
	}

	public Album(int id, String nom, String description, boolean prive, ArrayList<Image> images, User user) {
		super();
		this.id = id;
		this.nom = nom;
		this.description = description;
		this.prive = prive;
		this.images = images;
		this.user = user;
	}

	public Album(String nom, String description, boolean prive, ArrayList<Image> images, User user) {
		super();
		this.nom = nom;
		this.description = description;
		this.prive = prive;
		this.images = images;
		this.user = user;
	}

}
