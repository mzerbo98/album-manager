package beans;

import lombok.Data;

public @Data class User {

	private int id;
	private String username;
	private String password;
	private boolean admin;
	
	public User() {
		super();
	}
	
	public User(int id) {
		super();
		this.id = id;
	}	

	public User(String username, String password, boolean admin) {
		super();
		this.username = username;
		this.password = password;
		this.admin = admin;
	}
	
	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public User(int id, String username, boolean admin) {
		super();
		this.id = id;
		this.username = username;
		this.admin = admin;
	}	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return (id == other.id);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

}
