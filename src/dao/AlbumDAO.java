package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Album;
import beans.Image;
import beans.User;

public class AlbumDAO {
	private static final String	AJOUT_ALBUM_SQL	= "INSERT INTO album VALUES(0, ?, ?, ?, ?)";
	private static final String	SHARE_ALBUM_SQL	= "INSERT INTO acces VALUES(0, ?, ?)";
	private static final String	SELECT_ALBUMS_SQL	= "SELECT * FROM album";
	private static final String	SELECT_UTILISATEUR_SHARED_ALBUMS_SQL	= "SELECT a.* FROM album as a " +
																			"join acces as ac " +
																			"on a.id = ac.album " +
																			"where a.user=? " +
																			"or ac.user=? " +
																			"or a.prive=false";
	private static final String	SELECT_ALBUM_SQL	= "SELECT * FROM album where id = ?";
	private static final String	DELETE_ALBUM_SQL	= "DELETE FROM album where id = ?";

	public static void ajouter(Album album) throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		try
		{
			PreparedStatement statement = connexion
					.prepareStatement(AJOUT_ALBUM_SQL);
			statement.setString(1, album.getNom());
			statement.setString(2, album.getDescription());
			statement.setBoolean(3, album.isPrive());
			statement.setInt(4, album.getUser().getId());
			statement.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new DAOException("album non ajout�");
		}
	}
	
	public static void share(Album album,User u) throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		try
		{
			PreparedStatement statement = connexion
					.prepareStatement(SHARE_ALBUM_SQL);
			statement.setInt(1, album.getId());
			statement.setInt(2, u.getId());
			statement.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new DAOException("album non ajout�");
		}
	}
	
	public static void supprimer(Album album) throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		try
		{
			PreparedStatement statement = connexion
					.prepareStatement(DELETE_ALBUM_SQL);
			statement.setInt(1, album.getId());
			statement.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new DAOException("album non ajout�");
		}
	}
		public static List<Album> getList() throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		ArrayList<Album> albums = null;
		try
		{
			int id;
			albums = new ArrayList<Album>();
			String nom, description;
			boolean prive;
			ArrayList<Image> images = null;
			int uId;
			User u; 
			statement = connexion.createStatement();
			resultSet = statement.executeQuery(SELECT_ALBUMS_SQL);
			while (resultSet.next())
			{
				id = resultSet.getInt(1);
				nom = resultSet.getString(2);
				description = resultSet.getString(3);
				prive = resultSet.getBoolean(4);
				uId = resultSet.getInt(5);
				u = UserDAO.getUser(new User(uId));
				albums.add(new Album(id, nom, description, prive, images, u));
			}
		}
		catch (SQLException e)
		{
			throw new DAOException(e.getMessage());
		}
		return albums;
	}
		
		public static Album get(Album a) throws DAOException
		{
			Connection connexion = DatabaseManager.getConnection();
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			Album album = null;
			try
			{
				int id;
				String nom, description;
				boolean prive;
				ArrayList<Image> images = null;
				int uId;
				User u; 
				statement = connexion.prepareStatement(SELECT_ALBUM_SQL);
				statement.setInt(1, a.getId());
				resultSet = statement.executeQuery();
				while (resultSet.next())
				{
					id = resultSet.getInt(1);
					nom = resultSet.getString(2);
					description = resultSet.getString(3);
					prive = resultSet.getBoolean(4);
					uId = resultSet.getInt(5);
					u = UserDAO.getUser(new User(uId));
					album = new Album(id, nom, description, prive, images, u);
				}
			}
			catch (SQLException e)
			{
				throw new DAOException(e.getMessage());
			}
			return album;
		}
		
		public static ArrayList<Album> getUserSharedAlbums(User u) throws DAOException
		{
			Connection connexion = DatabaseManager.getConnection();
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			ArrayList<Album> albums = new ArrayList<Album>();
			try
			{
				int id;
				String nom, description;
				boolean prive;
				int uId;
				ArrayList<Image> images = null;
				statement = connexion.prepareStatement(SELECT_UTILISATEUR_SHARED_ALBUMS_SQL);
				statement.setInt(1, u.getId());
				statement.setInt(2, u.getId());
				resultSet = statement.executeQuery();
				while (resultSet.next())
				{
					id = resultSet.getInt(1);
					nom = resultSet.getString(2);
					description = resultSet.getString(3);
					prive = resultSet.getBoolean(4);
					uId = resultSet.getInt(5);
					u = UserDAO.getUser(new User(uId));
					albums.add(new Album(id, nom, description, prive, images, u));
				}
			}
			catch (SQLException e)
			{
				throw new DAOException(e.getMessage());
			}
			return albums;
		}
}
