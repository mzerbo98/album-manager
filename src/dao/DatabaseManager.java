package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseManager {
	private static final String	DB_URL		= "jdbc:mysql://localhost/album";
	private static final String	DB_USER		= "album";
	private static final String	DB_PASSWORD	= "album";

	private static Connection	connexion;

	private DatabaseManager()
	{
	}

	public static Connection getConnection() throws DAOException
	{
		if (connexion == null)
		{
			try
			{
				Class.forName("com.mysql.jdbc.Driver");
				Connection connexion = DriverManager.getConnection(DB_URL,
						DB_USER, DB_PASSWORD);
				return connexion;
			}
			catch (ClassNotFoundException e)
			{
				throw new DAOException("Erreur de chargement du pilote");
			}
			catch (SQLException e)
			{
				throw new DAOException("Erreur d'acc�s �la base de donn�es : "
						+ e.getMessage());
			}
		}
		return connexion;
	}
}
