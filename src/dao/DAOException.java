package dao;

public class DAOException extends Exception {

	private static final long serialVersionUID = 3678223761592324526L;

	public DAOException(String message){
		super(message);
	}
}
