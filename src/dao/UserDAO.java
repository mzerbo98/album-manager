package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Album;
import beans.Image;
import beans.User;

public class UserDAO {
	private static final String	AJOUT_UTILISATEUR_SQL	= "INSERT INTO user VALUES(0, ?,password(?), ?)";
	private static final String	SELECT_UTILISATEURS_SQL	= "SELECT * FROM user";
	private static final String	SELECT_UTILISATEUR_SQL	= "SELECT * FROM user where id=?";
	private static final String	SELECT_UTILISATEUR_ALBUMS_SQL	= "SELECT * FROM album where user=?";
	private static final String	CONNECT_UTILISATEUR_SQL	= "SELECT * FROM user where username = ? and password = password(?)";
	private static final String	DELETE_UTILISATEUR_SQL	= "DELETE FROM user where id = ?";

	public static void ajouter(User user) throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		try
		{
			PreparedStatement statement = connexion
					.prepareStatement(AJOUT_UTILISATEUR_SQL);
			statement.setString(1, user.getUsername());
			statement.setString(2, user.getPassword());
			statement.setBoolean(3, user.isAdmin());
			statement.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new DAOException("user non ajout�");
		}
	}
	
	public static void supprimer(User user) throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		try
		{
			PreparedStatement statement = connexion
					.prepareStatement(DELETE_UTILISATEUR_SQL);
			statement.setInt(1, user.getId());
			statement.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new DAOException("user non ajout�");
		}
	}
		public static List<User> getList() throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		ArrayList<User> users = null;
		try
		{
			int id;
			users = new ArrayList<User>();
			String nom;
			boolean admin;
			statement = connexion.createStatement();
			resultSet = statement.executeQuery(SELECT_UTILISATEURS_SQL);
			while (resultSet.next())
			{
				id = resultSet.getInt(1);
				nom = resultSet.getString(2);
				admin = resultSet.getBoolean(4);
				users.add(new User(id, nom, admin));
			}
		}
		catch (SQLException e)
		{
			throw new DAOException(e.getMessage());
		}
		return users;
	}
		
		public static User connect(User u) throws DAOException
		{
			Connection connexion = DatabaseManager.getConnection();
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			User user = null;
			try
			{
				int id;
				String nom;
				boolean admin;
				statement = connexion.prepareStatement(CONNECT_UTILISATEUR_SQL);
				statement.setString(1, u.getUsername());
				statement.setString(2, u.getPassword());
				resultSet = statement.executeQuery();
				while (resultSet.next())
				{
					id = resultSet.getInt(1);
					nom = resultSet.getString(2);
					admin = resultSet.getBoolean(4);
					user = new User(id, nom, admin);
				}
			}
			catch (SQLException e)
			{
				throw new DAOException(e.getMessage());
			}
			return user;
		}
		
		public static User getUser(User u) throws DAOException
		{
			Connection connexion = DatabaseManager.getConnection();
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			User user = null;
			try
			{
				int id;
				String nom;
				boolean admin;
				statement = connexion.prepareStatement(SELECT_UTILISATEUR_SQL);
				statement.setInt(1, u.getId());
				resultSet = statement.executeQuery();
				while (resultSet.next())
				{
					id = resultSet.getInt(1);
					nom = resultSet.getString(2);
					admin = resultSet.getBoolean(4);
					user = new User(id, nom, admin);
				}
			}
			catch (SQLException e)
			{
				throw new DAOException(e.getMessage());
			}
			return user;
		}
		
		public static ArrayList<Album> getUserAlbums(User u) throws DAOException
		{
			Connection connexion = DatabaseManager.getConnection();
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			ArrayList<Album> albums = new ArrayList<Album>();
			try
			{
				int id;
				String nom, description;
				boolean prive;
				int uId;
				ArrayList<Image> images = null;
				statement = connexion.prepareStatement(SELECT_UTILISATEUR_ALBUMS_SQL);
				statement.setInt(1, u.getId());
				resultSet = statement.executeQuery();
				while (resultSet.next())
				{
					id = resultSet.getInt(1);
					nom = resultSet.getString(2);
					description = resultSet.getString(3);
					prive = resultSet.getBoolean(4);
					uId = resultSet.getInt(5);
					u = UserDAO.getUser(new User(uId));
					albums.add(new Album(id, nom, description, prive, images, u));
				}
			}
			catch (SQLException e)
			{
				throw new DAOException(e.getMessage());
			}
			return albums;
		}
}
