package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.Album;
import beans.Image;

public class ImageDAO {
	private static final String	AJOUT_IMAGE_SQL	= "INSERT INTO image VALUES(0, ?, ?, ?, ?, ?, now(), now(), ?, ?)";
	private static final String	SELECT_IMAGES_SQL	= "SELECT * FROM image";
	private static final String	SELECT_IMAGE_SQL	= "SELECT * FROM image where id = ?";
	private static final String	DELETE_IMAGE_SQL	= "DELETE FROM image where id = ?";

	public static void ajouter(Image image) throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		try
		{
			PreparedStatement statement = connexion
					.prepareStatement(AJOUT_IMAGE_SQL);
			statement.setString(1, image.getTitre());
			statement.setString(2, image.getDescription());
			statement.setInt(3, image.getHauteur());
			statement.setInt(4, image.getLargeur());
			statement.setString(5, image.getKeywords());
			statement.setString(6,image.getUrl());
			statement.setInt(7, image.getAlbum().getId());
			statement.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new DAOException("image non ajout�");
		}
	}
	
	public static void supprimer(Image image) throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		try
		{
			PreparedStatement statement = connexion
					.prepareStatement(DELETE_IMAGE_SQL);
			statement.setInt(1, image.getId());
			statement.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new DAOException("image non ajout�");
		}
	}
		public static List<Image> getList() throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		ArrayList<Image> images = null;
		try
		{
			int id;
			images = new ArrayList<Image>();
			String titre, description;
			int aId;
			Album album = null;
			String keywords;
			Date creation;
			Date modification;
			int hauteur;
			int largeur;
			String url;
			statement = connexion.createStatement();
			resultSet = statement.executeQuery(SELECT_IMAGES_SQL);
			while (resultSet.next())
			{
				id = resultSet.getInt(1);
				titre = resultSet.getString(2);
				description = resultSet.getString(3);
				hauteur = resultSet.getInt(4);
				largeur = resultSet.getInt(5);
				keywords = resultSet.getString(6);
				creation = resultSet.getDate(7);
				modification = resultSet.getDate(8);
				url = resultSet.getString(9);
				aId = resultSet.getInt(10);
				album = AlbumDAO.get(new Album(aId));
				images.add(new Image(id, titre, description, keywords, creation, modification, hauteur, largeur, url, album));
			}
		}
		catch (SQLException e)
		{
			throw new DAOException(e.getMessage());
		}
		return images;
	}
		
		public static Image get(Image i) throws DAOException
		{
			Connection connexion = DatabaseManager.getConnection();
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			Image image = null;
			try
			{
				int id;
				String titre, description;
				int aId;
				Album album = null;
				String keywords;
				Date creation;
				Date modification;
				int hauteur;
				int largeur;
				String url;
				statement = connexion.prepareStatement(SELECT_IMAGE_SQL);
				statement.setInt(1, i.getId());
				resultSet = statement.executeQuery();
				while (resultSet.next())
				{
					id = resultSet.getInt(1);
					titre = resultSet.getString(2);
					description = resultSet.getString(3);
					hauteur = resultSet.getInt(4);
					largeur = resultSet.getInt(5);
					keywords = resultSet.getString(6);
					creation = resultSet.getDate(7);
					modification = resultSet.getDate(8);
					url = resultSet.getString(9);
					aId = resultSet.getInt(10);
					album = AlbumDAO.get(new Album(aId));
					image = new Image(id, titre, description, keywords, creation, modification, hauteur, largeur, url, album);
				}
			}
			catch (SQLException e)
			{
				throw new DAOException(e.getMessage());
			}
			return image;
		}
}
