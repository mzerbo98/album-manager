package metier.forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import beans.User;
import dao.UserDAO;
import dao.DAOException;

public class AjoutAlbumForm {
	private static final String	CHAMP_NOM			= "nom";

	private User	user;
	private String	statusMessage;
	private FormUtils utils;
	private Map<String, String>	messageErreurs = new HashMap<String, String>();

	public AjoutAlbumForm(HttpServletRequest request)
	{
		utils = new FormUtils(request,messageErreurs);

		String nom = utils.getParamater(CHAMP_NOM);

		// user = new User(nom, prenom, telephone);

		if (messageErreurs.isEmpty())
		{
			statusMessage = "Ajout effectu� avec succ�s";
		}
		else
		{
			statusMessage = "Echec de l'ajout de l'utilisateur";
		}
	}

	public User getClient() {
		return user;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public Map<String, String> getMessageErreurs() {
		return messageErreurs;
	}
	
	public boolean isValid()
	{
		return messageErreurs.isEmpty();
	}
	
}
