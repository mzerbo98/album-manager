package metier.forms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.DAOException;
import dao.UserDAO;

public class AuthenticationForm {

	private static final String	CHAMP_LOGIN		= "username";
	private static final String	CHAMP_PASSWORD	= "password";
	private HttpServletRequest	request;
	private String				login;
	private FormUtils utils;

	public AuthenticationForm(HttpServletRequest request)
	{
		this.request = request;
		utils = new FormUtils(request, null);
		
	}

	public User connect() throws DAOException
	{
		
		login = utils.getParamater(CHAMP_LOGIN);
		String password = utils.getParamater(CHAMP_PASSWORD);
		
		return UserDAO.connect(new User(login, password));
	}

	public String getLogin()
	{
		return login;
	}
}
